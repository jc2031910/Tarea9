#include <stdio.h>
struct Personas
{
    char Nombre[50];
    int Persona;
    int Edad;
    float Estatura;
} s[5];

int main()
{
    int a;

    printf("Proporcionar datos de la persona:\n");

    for(a=0; a<5; ++a)
    {
        s[a].Persona = a+1;

        printf("\nDatos de la persona %d\n",s[a].Persona);

        printf("Nombre: ");
        scanf("%s",s[a].Nombre);
        
         printf("Edad: ");
        scanf("%d",&s[a].Edad);

        printf("Estatura: ");
        scanf("%f",&s[a].Estatura);
            }

    printf("-------------------------------\nInformacion recabada de las personas:\n-------------------------------\n");
    for(a=0; a<5; ++a)
    {
        printf("Persona %d:\n",a+1);
        printf("Se llama: ");
        puts(s[a].Nombre);
        printf("Se edad es: %d años\n",s[a].Edad);
        printf("Mide: %.1f mts.\n",s[a].Estatura);
        printf("\n");
    }
    return 0;
}